package edu.westga.cs6241.babble;

import java.awt.Dimension;

import edu.westga.cs6241.babble.views.*;

/**
 * Main class for starting a Babble game. 
 * @author lewisb
 *
 * Edit by
 * @author Brandon Lee Upshaw
 * @version December 5th, 2016
 */
public class Babble {

	public static void main(String[] args) throws Exception {
		BabbleGUI run = new BabbleGUI();
		Dimension dimension = run.getPreferredSize();
		run.setSize(dimension);
		run.setVisible(true);
		
	}

}
