package edu.westga.cs6241.babble.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.westga.cs6241.babble.model.Tile;

/**
 * Tests the Tile class by trying to create specific tiles and verifying
 * 	that the values were stored correctly
 * @author Brandon Lee Upshaw - CS6241
 * @version October 24th, 2016
 *
 */
public class TestTileWhenCreateTile {
	Tile tile;
	/**
	 * Creates tile with letter 'F' and point value 4 - tests
	 * 	that both values are stored correctly
	 */
	@Test
	public void testCanMakeAFourPointFTile() {
		this.tile = new Tile('F', 4);
		assertTrue(this.tile.getLetter()=='F');
		assertTrue(this.tile.getPointValue()==4);
	}
	
	/**
	 * Tries to create tile with lower case 'h'
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testCanNotMakeATileWithLowerCaseH() {
		this.tile = new Tile('h', 4);
	}
	
	/**
	 * Tries to create tile with negative point value
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testCanNotMakeATileWithPointValueNegativeFour() {
		this.tile = new Tile('H', -4);
	}
	
	/**
	 * Tries to create tile with point value set to 0
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testCanNotMakeATileWithPointValueZero() {
		this.tile = new Tile('H', 0);
	}

}
