package edu.westga.cs6241.babble.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.westga.cs6241.babble.model.Tile;
import edu.westga.cs6241.babble.model.TileRack;

/**
 * Testing canMakeWordFrom method of TileRack class by building a rack with pre-defined
 * 	letters and verifying that words can or can not be made
 * @author Brandon Lee Upshaw - CS6241
 * @version October 24th, 2016
 *
 */
public class TestTileRackCanMakeWord {
	TileRack rack;
	Tile tile;
	
	/**
	 * Creates TileRack with 7 letters 'HEAVYHX'
	 */
	@Before
	public void setUp() {
		this.rack = new TileRack(7);
		this.tile = new Tile('H', 1);
		this.rack.append(tile);
		this.tile = new Tile('E', 1);
		this.rack.append(tile);
		this.tile = new Tile('A', 1);
		this.rack.append(tile);
		this.tile = new Tile('V', 1);
		this.rack.append(tile);
		this.tile = new Tile('Y', 1);
		this.rack.append(tile);
		this.tile = new Tile('H', 1);
		this.rack.append(tile);
		this.tile = new Tile('X', 1);
		this.rack.append(tile);	
	}
	
	/**
	 * Uses TileRack to test that 'HEAVY' can be made with current tiles
	 */
	@Test
	public void testTileRackShouldContainTheWordHeavy() {
		assertEquals(true, this.rack.canMakeWordFrom("HEAVY"));
	}
	
	/**
	 * Uses TileRack to verify that 'WET' can not be made with current tiles
	 */
	@Test
	public void testTileRackShouldNotContainTheWordWet() {
		assertEquals(false, this.rack.canMakeWordFrom("WET"));
	}
	
	/**
	 * Tests that word can not be made when rack is empty
	 */
	@Test
	public void testAndEmptyTileRackShouldNotContainTheWordDwell() {
		TileRack rack = new TileRack(7);
		assertTrue(rack.canMakeWordFrom("DWELL")==false);
	}
}
