package edu.westga.cs6241.babble.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.westga.cs6241.babble.model.Tile;
import edu.westga.cs6241.babble.model.Word;

/**
 * Test getScore method from Word class
 * @author Brandon Lee Upshaw 
 * @version October 24th, 2016
 *
 */
public class TestWordWhenGetScore {

	/**
	 * Creates word with tiles that spell "HEAVY" and verifies that point values add
	 * 	up to expected value of 14
	 */
	@Test
	public void testWordHeavyShouldHaveScoreOfFourteen() {
		Tile tile = new Tile('H', 4);
		Word word = new Word();
		word.append(tile);
		tile = new Tile('E', 1);
		word.append(tile);
		tile = new Tile('A', 1);
		word.append(tile);
		tile = new Tile('V', 4);
		word.append(tile);
		tile = new Tile('Y', 4);
		word.append(tile);
		assertTrue(word.getScore()==14);
	}
	
	/**
	 * Creates word object and verifies that there is no point value associated
	 */
	@Test
	public void testEmptyWordShouldHaveScoreOfZero() {
		Word word = new Word();
		assertTrue(word.getScore()==0);
	}

}
