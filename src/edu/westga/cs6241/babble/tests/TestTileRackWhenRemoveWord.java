package edu.westga.cs6241.babble.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.westga.cs6241.babble.model.Tile;
import edu.westga.cs6241.babble.model.TileRack;

/**
 * Test removeWord method of TileRack class to verify correct letters are removed
 * 	in different situations
 * @author Brandon Lee Upshaw - CS6241
 * @version October 24th, 2016
 *
 */
public class TestTileRackWhenRemoveWord {
	
	/**
	 * Creates TileRack with "HEAVYHX" and test that HEAVY is correctly removed
	 */
	@Test
	public void testShouldBeAbleToRemoveTheWordHeavy() {
		TileRack rack = new TileRack(7);
		Tile tile = new Tile('H', 1);
		rack.append(tile);
		tile = new Tile('E', 1);
		rack.append(tile);
		tile = new Tile('A', 1);
		rack.append(tile);
		tile = new Tile('V', 1);
		rack.append(tile);
		tile = new Tile('Y', 1);
		rack.append(tile);
		tile = new Tile('H', 1);
		rack.append(tile);
		tile = new Tile('X', 1);
		rack.append(tile);		
		rack.removeWord("HEAVY");
		assertTrue(rack.getNumberOfTilesNeeded()==5);
		assertTrue(rack.canMakeWordFrom("HEAVY")==false);
	}
	
	/**
	 * Creates TileRack with "CATAAAA" and removes "CAT" - verifies that
	 * 	correct letters remain with correct number of tiles
	 */
	@Test
	public void testShouldNotRemoveDuplicateTilesFromATileRack() {
		TileRack rack = new TileRack(7);
		Tile tile = new Tile('C', 1);
		rack.append(tile);
		tile = new Tile('A', 1);
		rack.append(tile);
		tile = new Tile('T', 1);
		rack.append(tile);
		tile = new Tile('A', 1);
		rack.append(tile);
		tile = new Tile('A', 1);
		rack.append(tile);
		tile = new Tile('A', 1);
		rack.append(tile);
		tile = new Tile('A', 1);
		rack.append(tile);
		rack.removeWord("CAT");
		assertTrue(rack.canMakeWordFrom("AAAA"));
		assertTrue(rack.getNumberOfTilesNeeded()==3);
	}
	
	/**
	 * Creates TileRack with "TAACAAA" and removes "CAT" - verifies that
	 * 	correct letters remain with correct number of tiles
	 */
	@Test
	public void testCanRemoveAWordFromTileRackWhenTilesOutOfOrder() {
		TileRack rack = new TileRack(7);
		Tile tile = new Tile('T', 1);
		rack.append(tile);
		tile = new Tile('A', 1);
		rack.append(tile);
		tile = new Tile('A', 1);
		rack.append(tile);
		tile = new Tile('C', 1);
		rack.append(tile);
		tile = new Tile('A', 1);
		rack.append(tile);
		tile = new Tile('A', 1);
		rack.append(tile);
		tile = new Tile('A', 1);
		rack.append(tile);
		rack.removeWord("CAT");
		assertTrue(rack.canMakeWordFrom("AAAA"));
		assertTrue(rack.getNumberOfTilesNeeded()==3);
	}
	
	

}
