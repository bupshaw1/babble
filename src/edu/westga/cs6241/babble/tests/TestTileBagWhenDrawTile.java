package edu.westga.cs6241.babble.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.westga.cs6241.babble.model.EmptyTileBagException;
import edu.westga.cs6241.babble.model.TileBag;

/**
 * Testing drawTile method from TileBag class and verifying that drawing a tile
 * 	removes it from the bag correctly.
 * @author Brandon Lee Upshaw - CS6241
 * @version October 24th, 2016
 *
 */
public class TestTileBagWhenDrawTile {

	
	private TileBag tileBag;
	
	/**
	 * Creates tileBag with 98 tiles
	 * @throws Exception	not used
	 */
	@Before
	public void setUp() throws Exception {
		this.tileBag = new TileBag();
	}
	
	/**
	 * Tests that when 98 tiles are drawn, bag is empty
	 * @throws EmptyTileBagException	not used
	 */
	@Test
	public void canDrawAllTilesFromTileBag() throws EmptyTileBagException {
		for(int i = 0; i < 98; i++) {
			this.tileBag.drawTile();
		}	
		assertEquals(true, this.tileBag.isEmpty());
	}
	
	/**
	 * Tests drawing more tiles than exist in bag
	 * @throws EmptyTileBagException
	 */
	@Test(expected = EmptyTileBagException.class)
	public void canNotDraw100TilesFromTileBag() throws EmptyTileBagException {
		for(int i = 0; i < 100; i++) {
			this.tileBag.drawTile();
		}
	}
}

