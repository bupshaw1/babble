package edu.westga.cs6241.babble.model;

import java.util.ArrayList;

public class TileRack
  extends TileGroup
{
  private int maxSize;
  
  public TileRack(int maxSize)
  {
    if (maxSize <= 0) {
      throw new IllegalArgumentException("maxSize must be greater than zero.");
    }
    this.maxSize = maxSize;
  }
  
  public boolean canMakeWordFrom(String text)
  {
    if (text.isEmpty()) {
      throw new IllegalArgumentException("text can not be empty");
    }
    ArrayList<Tile> copyOfTiles = new ArrayList<Tile>(super.getTiles());
    String textFound = "";
    for (int i = 0; i < text.length(); i++)
    {
      char next = text.charAt(i);
      for (Tile t : copyOfTiles) {
        if (next == t.getLetter())
        {
          textFound = textFound + next;
          copyOfTiles.remove(t);
          break;
        }
      }
    }
    return textFound.equals(text);
  }
  
  public Word removeWord(String text)
  {
    if (!canMakeWordFrom(text)) {
      throw new IllegalArgumentException("can not remove these tiles");
    }
    Word builtWord = new Word();
    for (int i = 0; i < text.length(); i++)
    {
      char next = text.charAt(i);
      for (Tile t : super.getTiles()) {
        if (next == t.getLetter())
        {
          builtWord.append(t);
          super.remove(t);
          break;
        }
      }
    }
    return builtWord;
  }
  
  public int getNumberOfTilesNeeded()
  {
    return this.maxSize - super.getTiles().size();
  }
}
