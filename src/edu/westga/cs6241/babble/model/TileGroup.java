package edu.westga.cs6241.babble.model;

import java.util.ArrayList;

/**
 * @author Brandon Lee Upshaw - CS6241
 * @version September 19th, 2016
 *
 */
public abstract class TileGroup {
	private ArrayList<Tile> tiles;
	
	public TileGroup() {
		this.tiles = new ArrayList<Tile>();
	}

	public void append(Tile tile) {
		if (tile == null) {
			throw new IllegalArgumentException("Tile can not be null.");
		}
		this.tiles.add(tile);
	}
	
	public void remove(Tile tile) {
		this.tiles.remove(tile);
		}
	
	public String toString() {
		String string = "";
		for (int index = 0; index < this.tiles.size(); index++) {
			string += this.tiles.get(index) ;
		}
		return string;
	}
	
	public ArrayList<Tile> getTiles() {
		return this.tiles;
	}
}
