/**
 * 
 */
package edu.westga.cs6241.babble.model;


/**
 * @author Brandon Lee Upshaw - CS6241
 * @version September 19th, 2016
 *
 */
public class Tile {
	private char letter;
	private int pointValue;
	
	public Tile(char letter, int pointValue) {
		if (letter < 'A' || letter > 'Z') {
			throw new IllegalArgumentException("Letter must be between A and Z.");
		}
		if (pointValue <= 0) {
			throw new IllegalArgumentException("Point Value must be larger than 0.");
		}
		this.letter = letter;
		this.pointValue = pointValue;
	}
	
	public char getLetter() {
		return this.letter;		
	}
	
	public int getPointValue() {
		return this.pointValue;
	}

}
