package edu.westga.cs6241.babble.model;
/**
 * @author Brandon Lee Upshaw - CS6241
 * @version September 19th, 2016
 *
 */
public class Word extends TileGroup {

	public Integer getScore() {
		int score = 0;
		for (int index = 0; index < super.getTiles().size(); index++) {
			score += super.getTiles().get(index).getPointValue();
		}
		return score;
	}
}
