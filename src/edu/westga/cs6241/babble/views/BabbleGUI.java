package edu.westga.cs6241.babble.views;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import edu.westga.cs6241.babble.controllers.BabbleController;
import edu.westga.cs6241.babble.model.Tile;

/**
 * GUI for Babble Game
 * 
 * @author Brandon Lee Upshaw
 * @version December 5th, 2016
 *
 */

public class BabbleGUI extends JFrame{
	private static final long serialVersionUID = 1L;
	private JButton[] tileButton;
	private JTextField field;
	private JButton reset;
	private JButton playWord;
	private JLabel scoreLabel;
	private BabbleController babbleController;
	
	public BabbleGUI() {
		this.startGUI();
		this.babbleController = new BabbleController();
		this.babbleController.startGame();
		this.babbleController.refreshTileRack();
		this.reloadTiles();
	}
	
	private void startGUI() {
		super.setDefaultCloseOperation(3);
		super.setTitle("Babble");
		
		Box main = new Box(1);
		this.add(main);
		Box tiles = new Box(0);
		main.add(tiles);
		Box word = new Box(0);
		main.add(word);
		
		this.tileButton = new JButton[7];
		for (int i = 0; i < this.tileButton.length; i++) {
			this.tileButton[i] = new JButton("@");
			tiles.add(this.tileButton[i]);
		}
		
		JButton[] selectedButton = this.tileButton;
		int a = selectedButton.length;
		int z = 0;
		while (z < a) {
			JButton placeHolder = selectedButton[z];
			placeHolder.addActionListener(new clickedButton());
			z++;
		}
		
		word.add(new JLabel("Your Word: "));
		this.field = new JTextField();
		this.field.setEditable(false);
		word.add(this.field);
		JPanel controlPanel = new JPanel(new FlowLayout(2));
		main.add(controlPanel);
		this.reset = new JButton("Reset");
		controlPanel.add(this.reset);
		controlPanel.add(Box.createHorizontalStrut(20));
		this.playWord = new JButton("Play Word");
		controlPanel.add(this.playWord);
		
		this.reset.addActionListener(new ClearAndSelectTile());
		this.playWord.addActionListener(new PlayWord());
		
		JPanel scoreBoard = new JPanel(new FlowLayout(2));
		main.add(scoreBoard);
		scoreBoard.add(Box.createHorizontalStrut(20));
		this.scoreLabel = new JLabel();
		this.setScore(0);
		scoreBoard.add(this.scoreLabel);
		super.pack();
		
	}
	
	private void addLetter(String letter) {
		String string = this.field.getText();
		string = String.valueOf(string) + letter;
		this.field.setText(string);
	}
	
	private class clickedButton implements ActionListener {
	@Override
	public void actionPerformed(ActionEvent event) {
		JButton selected = (JButton) event.getSource();
		String letter = "" + selected.getText().charAt(0);
		BabbleGUI.this.addLetter(letter);
		selected.setEnabled(false);
		}
	}
	
	private class ClearAndSelectTile implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			BabbleGUI.this.clearAndDeselect();
		}
	}
	
	private void clearAndDeselect() {
		JButton[] button = this.tileButton;
		int a = button.length;
		int z = 0;
		while (z < a) {
			JButton button2 = button[z];
			button2.setEnabled(true);
			z++;
		}
		this.field.setText("");
	}
	
	private class PlayWord implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			String word = BabbleGUI.this.field.getText();
			int checker = BabbleGUI.this.babbleController.checkCandidateWord(word);
			if (checker == 1) {
				JOptionPane.showMessageDialog(BabbleGUI.this,  "Invalid Word");	
			} else if (checker == 0) {
				BabbleGUI.this.babbleController.removeWord(word);
				BabbleGUI.this.babbleController.refreshTileRack();
				BabbleGUI.this.setScore(BabbleGUI.this.babbleController.getScore());
				BabbleGUI.this.clearAndDeselect();
				BabbleGUI.this.reloadTiles();
			}
		}
		
	}
	
	private void reloadTiles() {
		Tile[] tile = this.babbleController.getTiles();
		int i = 0;
		while (i < tile.length) {
			Tile current = tile[i];
			String letter = "" + current.getLetter();
			String points = "" + current.getPointValue();
			this.tileButton[i].setText(String.valueOf(letter) + " (" + points + ") ");
			i++;
		}
	}
	
	private void setScore(int score) {
		this.scoreLabel.setText("Total score: " + score);
	}
}


